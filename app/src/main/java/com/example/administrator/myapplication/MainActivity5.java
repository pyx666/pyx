package com.example.administrator.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

public class MainActivity5 extends AppCompatActivity {
        TextView mBtn;
        EditText mUserName,mPassWd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        mBtn = findViewById(R.id.t4);
        mUserName = findViewById(R.id.t6);
        mPassWd = findViewById(R.id.t9);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/login");

                String username = mUserName.getText().toString();
                String passwd = mPassWd.getText().toString();

                requestParams.addBodyParameter("usernaem",username);
                requestParams.addBodyParameter("passwd",passwd);
                x.http().post(requestParams, new Callback.CacheCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                       
                        Log.i("LoginActivity","success"+result);
                    }

                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {
                        Log.i("LoginActivity","fail"+ex.getMessage());
                    }

                    @Override
                    public void onCancelled(CancelledException cex) {

                    }

                    @Override
                    public void onFinished() {

                    }

                    @Override
                    public boolean onCache(String result) {
                        return false;
                    }
                });
            }
        });
    }
}
