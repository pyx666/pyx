package com.boxuegu.wujiang05.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.boxuegu.wujiang05.myapplication.model.LoginModel;
import com.google.gson.Gson;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

public class LoginActivity extends AppCompatActivity {
     TextView mBtn;
     EditText mUserName,mPasswd;
    TextView  register1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mUserName = findViewById(R.id.z7);
        mPasswd = findViewById(R.id.z9);
        register1 = findViewById(R.id.register1);
        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }});


        mBtn = findViewById(R.id.z6);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams requestParams =new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/login");
                String username = mUserName .getText().toString();
                String passwd = mPasswd.getText().toString();

                requestParams.addBodyParameter("username",username);
                requestParams.addBodyParameter("password",passwd);
                x.http().post(requestParams, new Callback.CacheCallback<String>() {

                    @Override
                    public void onSuccess(String result) {
                        Gson gson = new Gson();
                        LoginModel model = gson.fromJson(result,LoginModel.class);
                   Log.i("LoginActivity","success:"+result);
                       if(model.code==0 ){
                           Toast.makeText(LoginActivity.this,"登录成功",Toast.LENGTH_LONG).show();
                          //调到别的页面处
                       }else{
                           Toast.makeText(LoginActivity.this,"用户名和密码错误",Toast.LENGTH_LONG).show();
                       }
                   }

                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {
                        Log.i("LoginActivity","fail"+ex.getMessage());
                    }

                    @Override
                    public void onCancelled(CancelledException cex) {

                    }

                    @Override
                    public void onFinished() {

                    }

                    @Override
                    public boolean onCache(String result) {
                        return false;
                    }


        });
    }
});
}
}
