package com.boxuegu.wujiang05.myapplication;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.boxuegu.wujiang05.myapplication.frgment.MyFragment;
import com.boxuegu.wujiang05.myapplication.frgment.courseFragment;
import com.boxuegu.wujiang05.myapplication.frgment.exerciseFragment;

public class MainActivity extends AppCompatActivity {
   TextView mCoureseBtn,mExerciseBtn,myBtn;
   Fragment cuuresefragment = new courseFragment();
    Fragment exercisefragment = new exerciseFragment();
    Fragment myfragment = new MyFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.layout,cuuresefragment).commit();
        mExerciseBtn = findViewById(R.id.exercisebtn);
        mExerciseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,exercisefragment).commit();
                mCoureseBtn.setSelected(false);
                mExerciseBtn.setSelected(true);
                myBtn.setSelected(false);
            }
        });
        mCoureseBtn = findViewById(R.id.coursebtn);
        mCoureseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,cuuresefragment).commit();
                mCoureseBtn.setSelected(true);
                mExerciseBtn.setSelected(false);
                myBtn.setSelected(false);
            }
        });
        myBtn = findViewById(R.id.mybtn);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,myfragment).commit();
                mCoureseBtn.setSelected(false);
                mExerciseBtn.setSelected(false);
                myBtn.setSelected(true);

            }
        });
    }
}
