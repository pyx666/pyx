package com.boxuegu.wujiang05.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boxuegu.wujiang05.myapplication.model.LoginModel;
import com.google.gson.Gson;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

public class RegisterActivity extends AppCompatActivity {
     TextView mBtn;
     EditText mUsername,mPasswd1,mPasswd2;
     ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mBack = findViewById(R.id.back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mUsername = findViewById(R.id.q5);
        mPasswd1 = findViewById(R.id.q7);
        mPasswd2 = findViewById(R.id.q9);
        mBtn = findViewById(R.id.registerbtn);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mUsername.getText().toString().trim();
                String passwd1 = mPasswd1.getText().toString().trim();
                String passwd2 = mPasswd2.getText().toString().trim();
                if (TextUtils.isEmpty(username)) {
                    Toast.makeText(RegisterActivity.this, "用户名不能为空", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(passwd1)) {
                    Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(passwd2)) {
                    Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_LONG).show();
                    return;
                }
                RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/register");
                requestParams.addBodyParameter("username", username);
                requestParams.addBodyParameter("password", passwd1);
                requestParams.addBodyParameter("password", passwd2);
                x.http().post(requestParams, new Callback.CacheCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        Gson gson = new Gson();
                        LoginModel model = gson.fromJson(result, LoginModel.class);
                        Log.i("RegisterActivity", "success:" + result);
                        if (model.code == 0) {
                            Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_LONG).show();
                            //调到别的页面处
                        } else {
                            Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {
                        Log.i("RegisterActivity", "fail" + ex.getMessage());
                    }

                    @Override
                    public void onCancelled(CancelledException cex) {

                    }

                    @Override
                    public void onFinished() {

                    }

                    @Override
                    public boolean onCache(String result) {
                        return false;
                    }
                });
            }
        });
    }
}