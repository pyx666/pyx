package com.boxuegu.wujiang05.myapplication;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragemnt_exercise extends Fragment {
        ListView mListview;
        MyAdapter myAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragemnt_exercise, container, false);
        mListview = view.findViewById(R.id.listview);
        myAdapter = new MyAdapter();
        mListview.setAdapter(myAdapter);
        return view;
    }
        public class MyAdapter extends BaseAdapter{

            @Override
            public int getCount() {
                return 8;
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder viewHolder;
                if(convertView == null){
                    viewHolder = new ViewHolder();
                    convertView = View.inflate(getActivity(),R.layout.list_item_layout,null);
                    viewHolder.mPostionTv = convertView.findViewById(R.id.postiontext);
                    viewHolder.titleTv = convertView.findViewById(R.id.titletext);
                    viewHolder.countTv = convertView.findViewById(R.id.);

                }
                View view = View.inflate(getActivity(),R.layout.list_item_layout, null);
                return view;
            }
            public class ViewHolder{
                public TextView mPostionTv;
                public TextView titleTv;
                public TextView countTv;
            }
        }
}
