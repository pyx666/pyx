package com.boxuegu.wujiang05.myapplication.model;

public class LoginModel {
    public String msg;
    public  int code;
    public Date date;

    public class Date{
        public  int expire;
        public String token;
    }
}
